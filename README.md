# docker-angular2

A docker container bootstrapped to host the Angular 2 quickstart tutorial.

## Using docker-compose

```bash
# Runs the docker as a daemon.  It is accessible at http://localhost:22080
docker-compose up
```

## Using docker

### Building

```bash
# Creates a docker image with the name <owner>/<image> using the Dockerfile in the /web directory
docker build -t <owner>/<image> web/
```

### Running

```bash
# Runs the docker as a daemon.  It is accessible at http://localhost:22080
docker run -d -p 22080:8080 -t <owner>/<image>
```
