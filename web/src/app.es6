import {
	Component, Template, bootstrap
}
from 'angular2/angular2';

@
Component({
	selector: 'my-app' // Defines the <my-app></my-app> tag
})@ Template({
	inline: '<h1>Hello {{ name }}</h1>' // Defines the inline template for the component
})

class MyAppComponent {
	constructor() {
		this.name = 'Alice';
	}
}

bootstrap(MyAppComponent);
